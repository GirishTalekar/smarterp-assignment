const expect = require('chai').expect;
const link = require('../../links');

describe('Should create the links', () => {
  it('Create Shortlink', (done) => {
    var result = link.createShortLink('https://xyz.com');
    expect(result.urlHash).to.not.equal('');
    done();
  });
});
