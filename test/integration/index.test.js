const expect = require('chai').expect;
const server = require('./../../index');
const request = require('supertest')(server);

describe('Should create and verify the links', () => {
  it('Create and get the links', (done) => {
    request.post('/links').set('Accept', 'application/json').send({
      url: 'https://www.google.com/search?q=do+a+barrel+roll'
    }).end((err, res) => {
      if (err) done(err);
      expect(res.statusCode).to.equal(200);
      var result = JSON.parse(res.text);
      request.get(`/${result[0].urlHash}`).set('Accept', 'application/json').end((err, res) => {
        if (err) done(err);
        expect(res.statusCode).to.equal(302);
        done();
      });
    });
  });

  it('Shortlinks not found', (done) => {
    request.get('/abcdef').set('Accept', 'application/json').end((err, res) => {
      if (err) done(err);
      expect(res.statusCode).to.equal(404); done();
    });
  });
});
