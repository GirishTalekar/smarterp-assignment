const express = require('express');
const app = express();
const port = 3000;
const dotenv = require('dotenv');
dotenv.config();

const Shortlink = require('./db');

const bodyParser = require('body-parser');

app.use(bodyParser.json());

app.post('/links', async function (req, res) {
    if (!req.body.url) {
        res.status(404).send('Not found');
    }
    const result = await Shortlink.createShortlink(req.body);
    res.json(result);
});

app.all('/:shortUrl', function (req, res) {
    var shortUrl = req.params.shortUrl;
    Shortlink.getShortlink({ urlHash: shortUrl }).then((result) => {
        if (result && result.length > 0) {
            res.writeHead(302, { Location: result[0].url });
            res.end();
        } else {
            res.status(404).send('Not found');
        }
    });
});

app.listen(port, function () {
    console.log('Server started on port : ' + port);
});

module.exports = app;
