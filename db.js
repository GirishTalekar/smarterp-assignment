const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017/';
const link = require('./links');

function getConnection () {
    return new Promise((resolve, reject) => {
        MongoClient.connect(url, function (err, db) {
            if (err) throw reject(err);
            var dbo = db.db('local');
            resolve(dbo);
        });
    });
}

function createShortlink (req) {
    return new Promise((resolve, reject) => {
        getShortlink({ url: req.url }).then((shortLink) => {
            if (shortLink && shortLink.length > 0) {
                resolve(shortLink);
                return;
            }
            const newShortLink = link.createShortLink(req.url);

            getConnection().then((db) => {
                db.collection('Shortlink').insertOne(newShortLink, function (err, result) {
                    if (err) throw reject(err);
                    console.log('Document inserted..');
                    resolve(result);
                });
            });
        });
    });
}

function getShortlink (query) {
    return new Promise((resolve, reject) => {
        getConnection().then((db) => {
            db.collection('Shortlink').find(query).toArray(function (err, result) {
                if (err) throw reject(err);
                resolve(result);
            });
        });
    });
}

module.exports = { createShortlink, getShortlink };
