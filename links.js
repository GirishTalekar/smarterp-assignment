const short = require('short-uuid');

function createShortLink (url) {
    const shortenedURL = short.generate();
    return {
        url: url,
        urlHash: shortenedURL,
        shortUrl: `${process.env.SERVER_URL}/${shortenedURL}`
    };
}

module.exports = { createShortLink };
